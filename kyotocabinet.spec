Name:           kyotocabinet
Version:        1.2.80
Release:        1
Summary:        Straightforward implementation of DBM
License:        GPLv3
URL:            https://dbmx.net/kyotocabinet/
Source:         https://dbmx.net/kyotocabinet/pkg/%{name}-%{version}.tar.gz

Provides:       %{name}-libs = %{version}-%{release} %{name}-lib = %{version}-%{release}
Obsoletes:      %{name}-libs < %{version}-%{release} %{name}-lib < 1.2.76-3
BuildRequires:  gcc-c++ zlib-devel lzo-devel xz-devel

Patch0001:      enable-debugging-info.patch

%description
The kyotocabinet is a library of routines for managing databases.

%package        devel
Summary:        Development files for kyotocabinet
Requires:       %{name} = %{version}-%{release} pkgconfig

%description    devel
This package contains libraries and header files for developing applications that use kyotocabinet.

%package        help
Summary:        Documentation for kyotocabinet
BuildArch:      noarch
Provides:       %{name}-api-doc = %{version}-%{release} %{name}-apidocs = %{version}-%{release}
Obsoletes:      %{name}-api-doc < 1.2.76-3              %{name}-apidocs < %{version}-%{release}

%description    help
This package provides documentation for kyotocabinet.

%prep
%autosetup -n kyotocabinet-%{version} -p1

%build
%configure --disable-opt --enable-lzo --enable-lzma
%make_build

%install
%make_install
rm -rf %{buildroot}%{_defaultdocdir}

%check
#make check

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%doc ChangeLog COPYING FOSSEXCEPTION LINKEXCEPTION
%doc doc/{command.html,common.css,icon16.png}
%{_bindir}/*
%{_libdir}/libkyotocabinet.so.*
%exclude %{_libdir}/libkyotocabinet.a

%files devel
%{_includedir}/*
%{_libdir}/libkyotocabinet.so
%{_libdir}/pkgconfig/kyotocabinet.pc

%files help
%doc doc/api/* kyotocabinet.idl
%{_mandir}/man1/*

%changelog
* Thu Oct 19 2023 wulei <wu_lei@hoperun.com> - 1.2.80-1
- Update to 1.2.80

* Thu Jan 16 2020 lihao <lihao129@huawei.com> - 1.2.77-4
- Package init

